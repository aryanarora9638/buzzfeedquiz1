

import java.io.IOException;
import java.net.ServerSocket;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Scanner;

public class quiz {

    public static String conclusion(char response[]){
        //Ques 1
        int x = 0;
        if(response[0] == 'A'){ x = 2;}
        else if(response[0] == 'B'){ x = 3; }
        else if(response[0] == 'C'){ x = 3; }
        else if(response[0] == 'D'){ x = 3; }
        else {x = -1;}
        String statement = " Since you are taking " + x + " courses";
        //Ques 2
        int y = 0;
        if(response[2] == 'A'){ y = 1;}
        else if (response[2] == 'B'){ y = 2;}
        else if (response[2] == 'C'){ y = 3;}
        else {y = 0;}
        statement += " and you work " + y + " days a week, you should have " + (7-y) + " days to concentrate just on your studies." ;

        //Ques 3
        Double z = 1.0;
        if(response[3] == 'A'){ z = 1.0;}
        else if (response[3] == 'B'){ z = 2.0;}
        else if (response[3] == 'C'){ z = 3.0;}
        else if (response[4] == 'D'){ z = 4.0;}
        statement += " For these " + (7-y) + " days you can spend " + z + " hour each day for each of the " + x + " courses u took.";
        statement += " You can easily achieve this by dividing your study session into " + x + " multiple sessions of " +  (z/x) + " hours each.";

        //Ques 4
        String activity = "";
        if (response[5] == 'A'){ activity = "Sports"; }
        else if (response[5] == 'B'){ activity = "Watching TV"; }
        else if (response[5] == 'C'){ activity = "Playing computer games"; }
        else if (response[5] == 'D'){ activity = "Other"; }
        else { activity = "None"; }
        statement += " You still have a spare time for leisure acitvities for the rest of your " + (24 - z) + " hours of study."+ "Where you can  enjoy your favorite activity  i.e. " + activity;

        //Ques 5
        if (response[6] == 'A'){ activity = "Early bird"; }
        else if (response[6] == 'B'){ activity = "Night owl"; }
        else if (response[6] == 'C'){ activity = "comfortable either way"; }
        else if (response[6] == 'D'){ activity = "comfortable either way"; }
        else { activity = "None"; }
        statement += "Since you are " + activity + " YOU can plan your study sessions accordingly!";

        return  statement;
    }


    public static void main(String[] args) throws IOException {

        //Recording Responses
        char answers[] = new char[7];

        //Location  text file
        Path path = FileSystems.getDefault().getPath("textTest.txt");
        //System.out.println(path);

        //Scanner
        Scanner input = new Scanner(path); //reading text file
        Scanner sc = new Scanner(System.in); //reading user input

        for (int i = 0 ; i < answers.length ; i++){
            System.out.println(input.nextLine());
            while (!input.hasNextInt()){
                System.out.println(input.nextLine());
            }
            if (i == 0 || i == 2  || i == 3 || i == 5 || i == 6 ) {
                String option = sc.next();
                answers[i] = option.charAt(0);
            }

            if( (i == 1 && sc.next().equals("B")) || (i == 4 && sc.next().equals("B"))){
                answers[i] = 'F';
                //SKIPS NEXT QUES
                input.nextLine();
                while (!input.hasNextInt()){
                    input.nextLine();
                }
                i++;
            }
        }

        System.out.println(conclusion(answers));

    }
}



